<!DOCTYPE html >
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<jsp:include page="/common/includes/header.jsp"></jsp:include>
<title>Configure</title>
</head>
<body>
	<jsp:include page="/common/includes/navbar.jsp"></jsp:include>
	<div class="container-fluid" style="margin-bottom: 25px;">
		
		<ul class="nav nav-tabs" id="ulNavTab">
			<li role="presentation" onclick="showActiveConfigureTab('Feedback Schedule')"><a href="javascript:void(0)">Feedback Schedule</a></li>
			<li role="presentation" onclick="showActiveConfigureTab('Batch')"><a href="javascript:void(0)">Batch</a></li>
			<li role="presentation" onclick="showActiveConfigureTab('Faculty')"><a href="javascript:void(0)">Faculty</a></li>
			<li role="presentation" onclick="showActiveConfigureTab('User')"><a href="javascript:void(0)">User</a></li>
		</ul>
		
		<div class="container-fluid" id="divNavTabs" style="margin-top: 10px;">
			<div id="divFeedbackSchedule" class="hide">
				<div class="panel panel-default" style="width:50%; margin: 0 auto;">
					<div class="panel-heading"><h3 class="panel-title">Feedback Schedule List
						<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modelAddEdit"
							data-backdrop="static" style="margin-top: -8px;" data-callerbtn="Feedback Schedule-Add">
							Add <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
					</h3></div>
					<div class="panel-body" id="divFeedbackScheduleList"></div>
				</div>
			</div>
			<div id="divBatch" class="hide">
				<div class="panel panel-default" style="width:50%; margin: 0 auto;">
					<div class="panel-heading"><h3 class="panel-title">Batch List
						<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modelAddEdit"
							data-backdrop="static" style="margin-top: -8px;" data-callerbtn="Batch-Add">
							Add <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
					</h3></div>
					<div class="panel-body" id="divBatchList"></div>
				</div>
			</div>
			<div id="divFaculty" class="hide">
				<div class="panel panel-default" style="width:60%; margin: 0 auto;">
					<div class="panel-heading"><h3 class="panel-title">Faculty List
						<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modelAddEdit"
							data-backdrop="static" style="margin-top: -8px;" data-callerbtn="Faculty-Add">
							Add <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
					</h3></div>
					<div class="panel-body" id="divFacultyList"></div>
				</div>
			</div>
			<div id="divUser" class="hide">
				<div class="panel panel-default" style="width:70%; margin: 0 auto;">
				<div class="panel-heading"><h3 class="panel-title">User List
					<button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modelAddEdit"
						data-backdrop="static" style="margin-top: -8px;" data-callerbtn="User-Add">
						Add <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
				</h3></div>
				<div class="panel-body" id="divUserList"></div>
			</div>
			</div>
		</div>
		
	</div>
	
	<jsp:include page="/common/includes/footer.jsp"></jsp:include>
	<script type="text/javascript" src="script/js/configure.js"></script>
</body>
</html>