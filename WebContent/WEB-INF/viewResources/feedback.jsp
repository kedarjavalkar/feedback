<!DOCTYPE html >
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<jsp:include page="/common/includes/header.jsp"></jsp:include>
<title>Feedback</title>
</head>
<body>
<jsp:include page="/common/includes/navbar.jsp"></jsp:include>
	<div class="container-fluid" style="margin-bottom: 25px;">
	<form id="feedbackForm">
		
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading" style="padding-top: 15px;">
				<div style="width: 100%;">
					<div style="float: left; width: 30%;">
						<input id="studentName" class="form-control" type="text" placeholder="Student Name" autofocus required>
					</div>
					<div style="float: right; width: 15%;">
						<input id="studentBatchSuggest" class="form-control" type="text" placeholder="Batch" required>
					</div>
					<div style="margin: 0 auto; width: 20%;">
						<input id="feedbackScheduleSuggest" class="form-control" type="text" placeholder="Feedback Schedule" required>
					</div>
				</div>
			</div>
			<div class="panel-body" style="text-align: center; font-size: 18px; margin-top: -10px;">
				<span class="label label-default">Quantitative Feedback</span>
				<br>
				<span class="label label-success">#5: Strongly Agree</span> &nbsp;&nbsp;
				<span class="label label-primary">#4: Agree</span> &nbsp;&nbsp;
				<span class="label label-info">#3: Neutral</span> &nbsp;&nbsp;
				<span class="label label-warning">#2: Disagree</span> &nbsp;&nbsp;
				<span class="label label-danger">#1: Strongly Disagree</span>
				<br>
				<div style="width: 100%; padding-top: 20px;">
					<input type="text" class="form-control" id="facultySuggest" style="width: 25%; margin: 0 auto;" required>
				</div>
			</div>

			<div class="container-fluid" id="feedbackComments">
				<!-- Table -->
			</div>
		</div>
		<button style="display: none;" type="submit" id="btnSubmitFeedback"></button>
	</form>
	</div>
<jsp:include page="/common/includes/footer.jsp"></jsp:include>
<script type="text/javascript" src="script/js/feedback.js"></script>
	
</body>
</html>