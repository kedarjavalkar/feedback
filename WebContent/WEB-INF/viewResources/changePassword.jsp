

<form role="form" data-toggle="validator" id="formChangePassword">
	<table class="table table-condensed table-hover">
		<tr> <td> <div class="form-group">
					<label for="oldPassword" class="control-label">Old Password *</label>
					<input type="password" class="form-control" id="oldPassword" required>
				</div> </td> </tr>
		<tr> <td> <div class="form-group">
					<label for="newPassword" class="control-label">New Password *</label>
					<input type="password" class="form-control" id="newPassword" data-minlength="7" required>
					<span class="help-block">Minimum 7 characters</span>
				</div> </td> </tr>
		<tr> <td> <div class="form-group">
					<label for="confirmPassword" class="control-label">Confirm Password *</label>
					<input type="password" class="form-control" id="confirmPassword" data-match="#newPassword" 
						data-match-error="Whoops, these don't match !" data-error=" " required>
					<div class="help-block with-errors"></div>
				</div> </td> </tr>
		<tr style="display: none;"> <td> <div class="form-group">
    				<button type="submit" id="btnChangePassword">Submit</button>
  				</div> </td> </tr>
  		<tr class="pull-right"><td>* <span style="font-size: 10px;">Required field</span></td></tr>
	</table>
</form>


<script src="script/js/changePassword.js"></script>

