<!DOCTYPE html >
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<jsp:include page="/common/includes/header.jsp"></jsp:include>
<link rel="stylesheet" href="script/css/login.css">
<title>Login</title>
</head>
<body>
	<div class="container-fluid" style="margin-bottom: 25px;">

		<form class="form-signin" id="loginForm">
			<!-- <div class="circularImg"></div> -->
			<input id="inputEmail" class="form-control" type="text" placeholder="Username" autofocus required>
			<input id="inputPassword" class="form-control" type="password" placeholder="Password" required>
			<!-- <div class="checkbox">
				<label> <input type="checkbox" value="remember-me"> Remember me </label>
			</div> -->
			<button id="buttonSignIn" class="btn btn-lg btn-primary btn-block"> Login </button>
		</form>

	</div>
<jsp:include page="/common/includes/footer.jsp"></jsp:include>
<script type="text/javascript" src="script/js/login.js"></script>
</body>
</html>


