

<form role="form" data-toggle="validator" id="formBatchAddEdit">
	<table class="table table-condensed table-hover">
		<tr> <td> <div class="form-group">
					<label for="inputName" class="control-label">Name *</label>
					<input type="text" class="form-control" id="inputName" style="width: 50%;" required>
					<div class="help-block with-errors"></div>
				</div> </td> </tr>
		<tr> <td> <div class="form-group">
				<label for="isActive" class="control-label">Is Active :&nbsp;&nbsp;</label>
				<input type="checkbox" id="isActive">
				</div> </td> </tr>
		<tr style="display: none;"> <td> <div class="form-group">
    				<button type="submit" id="btnBatchAddEdit">Submit</button>
  				</div> </td> </tr>
  		<tr class="pull-right"><td>* <span style="font-size: 10px;">Required field</span></td></tr>
	</table>
</form>


<script src="script/js/addEditBatch.js"></script>

