

<form role="form" data-toggle="validator" id="formUserAddEdit">
	<table class="table table-condensed table-hover">
		<tr><td> <div class="form-group">
					<label for="inputFirstName" class="control-label">First Name *</label>
					<input type="text" class="form-control" id="inputFirstName" required>
					<div class="help-block with-errors"></div>
				</div> </td>
			<td> <div class="form-group">
				<label for="inputMiddleName" class="control-label">Middle Name</label>
				<input type="text" class="form-control" id="inputMiddleName" >
				<div class="help-block with-errors"></div>
				</div> </td>
			<td> <div class="form-group">
				<label for="inputLastName" class="control-label">Last Name *</label>
				<input type="text" class="form-control" id="inputLastName" required>
				<div class="help-block with-errors"></div>
				</div> </td> </tr>
		<tr><td colspan="2"> <div class="form-group">
				<label for="inputLoginId" class="control-label">Login Id *</label>
				<input type="email" class="form-control" id="inputLoginId" required>
				<div class="help-block with-errors"></div>
				</div> </td> 
			<td> <div class="form-group">
				<label for="inputLoginId" class="control-label">Password</label>
				<input type="text" class="form-control" id="password" value="********" disabled>
				<div class="help-block with-errors"></div>
				</div> </td></tr>
		<tr> <td colspan="2"> <div class="form-group">
					<label for="isActive" class="control-label">Is Active :&nbsp;&nbsp;</label>
					<input type="checkbox" id="isActive">
				</div> </td> 
			<td><span class="pull-right">* <span style="font-size: 10px;">Required field</span></span></td> </tr>
		<tr style="display: none;"> <td colspan="2"> <div class="form-group">
    			<button type="submit" id="btnUserAddEdit">Submit</button>
  			</div> </td> </tr>
	</table>
</form>


<script src="script/js/addEditUser.js"></script>

