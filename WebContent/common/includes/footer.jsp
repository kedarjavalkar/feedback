

<!-- JS Start -->
<!-- JQuery / Magic JS moved with CSS -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.mcustomscrollbar/3.0.7/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="http://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="common/includes/script/validator.min.js"></script> <!-- 1000hz.github.io/bootstrap-validator/ -->
<script type="text/javascript" src="common/includes/script/magicsuggest-min.js"></script> <!-- http://nicolasbize.com/magicsuggest/ -->
<script type="text/javascript" src="common/includes/script/bootstrap-datepicker.min.js"></script> <!-- http://bootstrap-datepicker.readthedocs.org -->
<script type="text/javascript" src="common/includes/script/jquery.confirm.min.js"></script> <!-- http://myclabs.github.io/jquery.confirm/ -->
<script type="text/javascript" src="common/includes/script/excellentexport.min.js"></script> <!-- https://github.com/jmaister/excellentexport -->

<!-- <div class="magicFoot">
	<p> Developed Under <a href="http://spring-kedarjavalkar.rhcloud.com/" target="_blank">Spring &amp; Hibernate</a> ! </p>
</div> -->

<jsp:include page="/common/includes/support.jsp"></jsp:include>
