/*$('document').ready(function(){
	$('html').mCustomScrollbar({
		scrollInertia: 0,
		theme:'minimal-dark',
		advanced:{ 
			updateOnSelectorChange:true,
			updateOnContentResize:true
		},
	});
});
 */
//$('body').data("bar", "foobar" ); //alert($("body").data("bar"));

function magicActiveNavLink(link){
	$('#divNavBar ul li').each(function(){
		$(this).removeClass('active');
		if(link==$(this).text()){
			$(this).addClass('active');
		}
	});
}

function magicStartBlur(){
	$('body div').each(function(){
		$(this).addClass('magicBlur');
	});
	$('#loadingDiv').removeClass('magicBlur');
	$('#loadingDiv').removeClass('hide');
	
}
function magicStopBlur(){
	$('body div').each(function(){
		$(this).removeClass('magicBlur');
	});
	$('#loadingDiv').addClass('hide');
}

function magicSuccess(str){
	$('#magicSuccess').html('<strong>Success! </strong>'+str).removeClass('hide');
	setTimeout(function(){
		$('#magicSuccess').html('').addClass('hide');
	},9999);
}

function magicInfo(str){
	$('#magicInfo').html('<strong>Note! </strong>'+str).removeClass('hide');
	setTimeout(function(){
		$('#magicInfo').html('').addClass('hide');
	},9999);
}

function magicError(str){
	$('#magicError').html('<strong>Error! </strong>'+str).removeClass('hide');
	setTimeout(function(){
		$('#magicError').html('').addClass('hide');
	},9999);
}

function magicScrollBar(element){

	$(element).mCustomScrollbar({
		scrollInertia: 0,
		theme:'minimal-dark',
		advanced:{
			updateOnSelectorChange:true,
			updateOnContentResize:true
		},
	});
}

function magicTableBody(jsonArray){
	//'<tbody>'
	var str='';
	$.each(jsonArray, function() {
		str+='<tr>';
		$.each(this, function(key, value) {
			str+='<td>'+value+'</td>';
		});
		str+='</tr>';
	});
	return str;
}
