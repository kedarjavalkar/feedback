

<div id="loadingDiv" class="alert well well-sm hide in" role="alert">
	<span class="rect1"></span> <span class="rect2"></span>
	<span class="rect3"></span> <span class="rect4"></span>
	<span class="rect5"></span> <span class="rect6"></span>
	<span class="rect7"></span> <span class="rect8"></span>
	<p>Loading...</p>
</div>

<div id="magicSuccess" class="alert alert-success hide magicAlert" role="alert"></div>
<div id="magicInfo" class="alert alert-info hide magicAlert" role="alert"></div>
<div id="magicError" class="alert alert-danger hide magicAlert" role="alert"></div>
<div id="exportExcelDiv" class="hide"></div>

<!-- Model -->
<div class="modal fade" id="modelAddEdit" tabindex="-1" role="dialog" aria-labelledby="modelAddEditHeader" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="modelAddEditHeader">Header</h4>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="btnAddEditModel">Add/Edit</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

$('document').ready(function(){
	$('#modelAddEdit').on('hidden.bs.modal', function(e) {
		$(this).find('.modal-body').html('');
		$(this).find('#modelAddEditHeader').text('Header');
		$(this).find('#btnAddEditModel').text('Add/Edit');
		$("#btnAddEditModel").unbind("click");
	});	
});

</script>

<!-- <style type="text/css">
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    height: 80%;
    overflow-y: auto;
}
</style> -->