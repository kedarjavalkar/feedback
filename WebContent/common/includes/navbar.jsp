<script type="text/javascript">
$.ajax({
	url : 'authenticate.io',
	type : 'POST',
	dataType : 'text',
	cache: false,
	success : function(result) {
		if (result == 'Failure')
			window.location.href = 'index.jsp';
	},
});
</script>
		
<!-- Navbar Start -->
<div class="container-fluid">
	<div class="navbar navbar-inverse" role="navigation">
	
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="Feedback.io">
				<span class="glyphicon glyphicon-user"></span>
				<%= session.getAttribute("mecca-feedback-user") %>
			</a>
		</div>
		
		<div class="navbar-collapse collapse" id="divNavBar">
			<%-- <ul class="nav navbar-nav">
				<li id="sessionUser"><a href="javascript:void(0)">
					<span class="glyphicon glyphicon-user"></span>
						<%= session.getAttribute("mecca-feedback-user") %>
				</a></li>
				<!-- <li><a href="#about">About</a></li> -->
			</ul> --%>
			
			<ul class="nav navbar-nav navbar-right">
				<% if (session.getAttribute("mecca-feedback-role-report") != null) { %>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" 
						role="button" aria-expanded="false">
						<span class="glyphicon glyphicon-list-alt"></span> Reports<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li onclick="rawReport()"><a href="javascript:void(0)">Raw</a></li>
						<li onclick=""><a href="javascript:void(0)">Report#2</a></li>
						<li onclick=""><a href="javascript:void(0)">Report#3</a></li>
					</ul>
				</li> <% } %>
				<% if (session.getAttribute("mecca-feedback-role-config") != null) { %>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" 
						role="button" aria-expanded="false">Configure<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li onclick="setActiveConfigureTab(this)"><a href="Configure.io">Feedback Schedule</a></li>
						<li onclick="setActiveConfigureTab(this)"><a href="Configure.io">Batch</a></li>
						<li onclick="setActiveConfigureTab(this)"><a href="Configure.io">Faculty</a></li>
						<li onclick="setActiveConfigureTab(this)"><a href="Configure.io">User</a></li>
					</ul>
				</li> <% } %>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" 
						role="button" aria-expanded="false">
						<span class="glyphicon glyphicon-cog"></span> <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						 <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modelAddEdit"
							data-backdrop="static" onclick="loadChangePassword()">Change Password</a></li>
						<li onclick="logoff()"><a href="javascript:void(0)"><span class="glyphicon glyphicon-off"></span> Logoff</a></li>
					</ul>
				</li>
			</ul>
		</div> <!--/.nav-collapse -->
		
	</div>
</div> 
 <!-- Navbar End -->

<script type="text/javascript">
	$('document').ready(function(){

		magicActiveNavLink('dummyLink');
		
		if (sessionStorage.getItem("activeConfigureTab")!=null){
			showActiveConfigureTab(sessionStorage.getItem("activeConfigureTab"));
			sessionStorage.removeItem("activeConfigureTab")
		}
		
	});
	
	function setActiveConfigureTab(eThis){
		sessionStorage.setItem("activeConfigureTab",$(eThis).text());
	}
	
	function logoff() {
		$.ajax({
			url : 'logout.io',
			type : 'POST',
			dataType : 'text',
			cache: false,
			beforeSend:function(){ magicStartBlur(); },
			success : function(result) {
				if(result == "Success")
					window.location.href = 'index.jsp';
			},
		});
	}
	
	function loadChangePassword() {
		$('#modelAddEdit').find('#btnAddEditModel').text('Update');
		$('#modelAddEdit').find('#modelAddEditHeader').text('Change Password');
		$('#modelAddEdit').find('.modal-body').load('changePassword.io');
	}

</script>

<% if (session.getAttribute("mecca-feedback-role-report") != null) { %>
	<script type="text/javascript" src="script/js/exportReport.js"></script>
<% } %>


