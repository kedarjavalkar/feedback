

function rawReport() {

	$.ajax({
		url:'rawExcelReport.io',
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function(){ magicStartBlur(); },
		success:function(jsonArray){
			var str = '<table id="exportMeTbl"><thead><tr><th>Sr.No.</th><th>Student</th><th>Batch</th><th>Schedule</th><th>Faculty</th><th>Comment</th>'+
			'<th>Physics</th><th>Chemistry</th><th>Math</th><th>Biology</th><th>Created Date</th><th>Created By</th></tr></thead>';
			str += '<tbody>'+magicTableBody(jsonArray);
			str += '<tbody></table>';
			
			'<br><a download="Raw.csv" href="#" onclick="return ExcellentExport.csv(this, \'exportMeTbl\');" id="exportExcelNow">Export to CSV</a>';
			$('#exportExcelDiv').html(str);
			magicInfo('Export to CSV <a download="Raw.csv" href="#" onclick="return ExcellentExport.csv(this, \'exportMeTbl\');" id="exportExcelNow">'+
					'<u style="color:red;">Click!<u></a>');
			setTimeout(function(){
				$('#exportExcelDiv').html('');
			},9999);
		},
		error:function(){ magicError("Calling 'rawExcelReport.io' "); },
		complete:function(){ magicStopBlur(); },
	});








}