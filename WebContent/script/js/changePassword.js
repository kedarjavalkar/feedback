$('document').ready(function() {

	$('#btnAddEditModel').bind('click', function() {
		$('#btnChangePassword').click();
	});
	
	$('#formChangePassword').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			// do something when error
		} else {
			e.preventDefault(); //preventing form submit
			updatePassword();
		}
	});
	
});

function updatePassword() {
	
	var oldPassword = $('#oldPassword').val();
	var newPassword = $('#newPassword').val();
		
	$.ajax({
		url : 'updatePassword.io',
		type : 'POST',
		data :{oldPassword:oldPassword,newPassword:newPassword},
		dataType : 'json',
		cache: false,
		success : function(json) {
			$.each(json, function(key, value) {
				if(key=="Error")
					magicError(value);
				else if(key=="Success") {
					$('#modelAddEdit').modal('hide');
					magicSuccess(value);
				}
			});
		},
		error : function() {
			magicError('Calling "changePassword.io"');
		},
	});
	
}