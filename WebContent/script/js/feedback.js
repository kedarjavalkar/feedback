$('document').ready(function(){

	// get feedback schedule list
	$.ajax({
		url:'getFeedbackScheduleList.io',
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function(){ magicStartBlur(); },
		success:function(jsonArray){
			var feedbackScheduleSuggest = $('#feedbackScheduleSuggest').magicSuggest({
				typeDelay: 0,
				//method:'GET',
				//queryParam: 'k',
				data:jsonArray,
				valueField: 'feedbackScheduleId',
				displayField: 'feedbackSchedule',
				allowFreeEntries:false,
				selectFirst:true,
				maxSelection:1,
				//maxSuggestions:8,
				placeholder:'Faculty',
				//minChars:3, minCharsRenderer:function(v){return'Be more precise!!';},
				//maxEntryLength: 5, maxEntryRenderer: function(v){return 'Too Long!!';},
				noSuggestionText: 'No result matching the term <strong>{{query}}</strong>',
				maxDropHeight: 145,
				useZebraStyle: true,
				toggleOnClick: true,
			});
			$(feedbackScheduleSuggest).on('selectionchange', function(){
				$('#feedbackScheduleSuggest').data("feedbackScheduleSuggest",this.getValue());
			});
		},
		error:function(){ magicError("Calling 'getFeedbackScheduleList.io' "); },
		complete:function(){ magicStopBlur(); },
	});

	// get batch list
	$.ajax({
		url:'getBatchList.io',
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function(){ magicStartBlur(); },
		success:function(jsonArray){
			var studentBatchSuggest = $('#studentBatchSuggest').magicSuggest({
				typeDelay: 0,
				//method:'GET',
				//queryParam: 'k',
				data:jsonArray,
				valueField: 'batchId',
				displayField: 'batchName',
				allowFreeEntries:false,
				selectFirst:true,
				maxSelection:1,
				//maxSuggestions:8,
				placeholder:'Faculty',
				//minChars:3, minCharsRenderer:function(v){return'Be more precise!!';},
				//maxEntryLength: 5, maxEntryRenderer: function(v){return 'Too Long!!';},
				noSuggestionText: 'No result matching the term <strong>{{query}}</strong>',
				maxDropHeight: 145,
				useZebraStyle: true,
				toggleOnClick: true,
			});
			$(studentBatchSuggest).on('selectionchange', function(){
				$('#studentBatchSuggest').data("studentBatchSuggest",this.getValue());
			});
		},
		error:function(){ magicError("Calling 'getBatchList.io' "); },
		complete:function(){ magicStopBlur(); },
	});

	// get faulty list
	$.ajax({
		url:'getFacultyList.io',
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function(){ magicStartBlur(); },
		success:function(jsonArray){
			var facultySuggest = $('#facultySuggest').magicSuggest({
				typeDelay: 0,
				//method:'GET',
				//queryParam: 'k',
				data:jsonArray,
				valueField: 'facultyId',
				displayField: 'facultyName',
				allowFreeEntries:false,
				selectFirst:true,
				maxSelection:1,
				//maxSuggestions:8,
				placeholder:'Faculty',
				//minChars:3, minCharsRenderer:function(v){return'Be more precise!!';},
				//maxEntryLength: 5, maxEntryRenderer: function(v){return 'Too Long!!';},
				noSuggestionText: 'No result matching the term <strong>{{query}}</strong>',
				maxDropHeight: 145,
				useZebraStyle: true,
				toggleOnClick: true,
			});
			$(facultySuggest).on('selectionchange', function(){
				$('#facultySuggest').data("facultySuggest",this.getValue());
			});
		},
		error:function(){ magicError("Calling 'getFacultyList.io' "); },
		complete:function(){ magicStopBlur(); },
	});

	getFeedbackComments();

});

function getFeedbackComments() {

	$.ajax({
		url:'getfeedbackCommentList.io',
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function(){
			magicStartBlur();
		},
		success:function(jsonArray){

			var str='<table id="tblFeedbackComment" class="table table-condensed table-hover">'+
			'<thead><tr><th>#</th><th>Comment</th>'+
			'<th style="width:100px;text-align:center;">Physics</th><th style="width:100px;text-align:center;">Chemistry</th>'+
			'<th style="width:100px;text-align:center;">Math</th><th style="width:100px;text-align:center;">Biology</th>'
			+'</tr></thead>';
			str+='<tbody>';
			var i=0;
			var feedbackId;
			$.each(jsonArray, function() {
				str+='<tr id='+this['feedbackId']+'>';
				$.each(this, function(key, value) {
					if(key=='feedbackId'){
						str+='<td>'+parseInt(++i)+'</td>';
						feedbackId=value;
					}else if(key=='comment'){
						str+='<td id='+feedbackId+'>'+value+'</td>';
					}
				});
				var tempStr = '<td><input type="text" class="form-control points" maxlength="1" '+
				'style="text-align:center" onkeypress="return isNumber(event)" required></td>';
				str+=tempStr+tempStr+tempStr+tempStr;
				str+='</tr>';
			});
			str+='</tbody>';
			str+='</table>';
			str+= '<div style="margin:0 auto;width:100px"> <button type="button" class="btn btn-primary btn-lg" '+
			'id="btnSaveFeedback">&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;</button> </div> <br>'

			$('#feedbackComments').html(str);
			$('#btnSaveFeedback').bind('click', function() {
				$('#btnSubmitFeedback').click();
			});

			var inputs = document.querySelectorAll('.points'), i;
			for(i = inputs.length - 1; i >= 0; i--) {
				inputs[i].onkeydown = checkNumber;
				inputs[i].onkeyup   = checkFilled;
			}
		},
		error:function(){
			magicError("Calling 'getfeedbackCommentList.io' ");
		},
		complete:function(){
			magicStopBlur();
		},
	});
}

var validChar;
function checkNumber(event) {
	event = (event) ? event : window.event;
	var charCode = (event.which) ? event.which : event.keyCode;
	if (!(charCode >= 48 && charCode <= 53) 
			&& !(charCode >= 96 && charCode <= 101)
			&& !(charCode == 16 || charCode == 9)) {
		event.preventDefault();
		return false;
	} else if(!(charCode == 16 || charCode == 9)) {
		validChar = true;
	}
}
function checkFilled(event) {
	if(validChar && event.target.value.length === 1) {
		validChar = false;
		nextInput(event.target.parentNode);
	}
}
function nextInput(el) {
	if(el.nextElementSibling) {
		if(!el.nextElementSibling.firstElementChild) {
			nextInput(el.nextElementSibling);
			return false;
		}
		var nextSibling = el.nextElementSibling.firstElementChild;
	} else {
		if(!el.parentNode.nextElementSibling) {
			return false;
		}
		var nextRow = el.parentNode.nextElementSibling;
		if(!nextRow.firstElementChild.firstElementChild) {
			nextInput(nextRow.firstElementChild);
			return false;
		}
		var nextSibling = nextRow.firstElementChild.firstElementChild;
	}
	var nextType = nextSibling.tagName.toLowerCase();
	if(nextType !== "input") {
		nextInput(nextSibling);
		return false;
	}
	nextSibling.focus();
}




$('#feedbackForm').on('submit', function(e) {
	if (e.isDefaultPrevented()) {
		return;
	} 

	e.preventDefault();
	var feedbackSchedule = $('#feedbackScheduleSuggest').data("feedbackScheduleSuggest");
	var studentBatch = $('#studentBatchSuggest').data("studentBatchSuggest");
	var faculty = $('#facultySuggest').data("facultySuggest");

	if(studentBatch==null || studentBatch==undefined || studentBatch==''){
		$('#studentBatchSuggest').find('input').focus();
		return;
	}
	if(feedbackSchedule==null || feedbackSchedule==undefined || feedbackSchedule==''){
		$('#feedbackScheduleSuggest').find('input').focus();
		return;
	}
	if(faculty==null || faculty==undefined || faculty==''){
		$('#facultySuggest').find('input').focus();
		return;
	}

	var studentName = $('#studentName').val().trim();
	if(studentName.length < 1){
		$('#studentName').val('');
		$('#studentName').focus();
		return;
	}

	var facultyFeedback=[];
	$("#tblFeedbackComment tbody").find('tr').each(function (i, el) {

		var batchObj = new Object();
		batchObj.id = studentBatch[0];
		var feedbackScheduleObj = new Object();
		feedbackScheduleObj.id = feedbackSchedule[0];
		var facultyObj = new Object();
		facultyObj.id = faculty[0];
		var feedbackObj = new Object();
		feedbackObj.id = $(this).attr("id");

		var facultyFeedbackObj = new Object();
		facultyFeedbackObj.studentName = studentName;
		facultyFeedbackObj.batch = batchObj;
		facultyFeedbackObj.feedbackSchedule = feedbackScheduleObj;
		facultyFeedbackObj.faculty = facultyObj;
		facultyFeedbackObj.feedback = feedbackObj;
		facultyFeedbackObj.physics = $(this).find('td:nth-child(3)').find('input').val();
		facultyFeedbackObj.chemistry = $(this).find('td:nth-child(4)').find('input').val();
		facultyFeedbackObj.math = $(this).find('td:nth-child(5)').find('input').val();
		facultyFeedbackObj.biology = $(this).find('td:nth-child(6)').find('input').val();
		facultyFeedback.push(JSON.stringify(facultyFeedbackObj));
	});

	$.ajax({
		url:'saveFacultyFeedback.io',
		type:'POST',
		data: {facultyFeedback:facultyFeedback},
		dataType:'text',
		cache: false,
		beforeSend:function(){ magicStartBlur(); },
		success:function(msg){
			magicSuccess(msg);
			$("html, body").animate({ scrollTop: 0 }, "slow");
			$('.points').val('');
			$('#studentName').val('');
			$('#studentName').focus();
		},
		error:function(){ magicError("Calling 'saveFacultyFeedback.io' "); },
		complete:function(){ magicStopBlur(); },
	});

});
