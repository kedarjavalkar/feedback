$('document').ready(function(){

	magicActiveNavLink('dummyLink');
	
	if (sessionStorage.getItem("activeConfigureTab")!=null){
		showActiveConfigureTab(sessionStorage.getItem("activeConfigureTab"));
		sessionStorage.removeItem("activeConfigureTab")
	}
	
	$('#modelAddEdit').on('shown.bs.modal',function(event){

		var button = $(event.relatedTarget);
		var callerBtn = button.data('callerbtn');
		
		//$(this).find('.modal-dialog').addClass('modal-sm');
		if(callerBtn.split('-')[0]=='Feedback Schedule'){
			$(this).find('.modal-body').load('addEditFeedbackSchedule.io');
		}
		else if(callerBtn.split('-')[0]=='Batch'){
			$(this).find('.modal-body').load('addEditBatch.io');
		}
		else if(callerBtn.split('-')[0]=='Faculty'){
			$(this).find('.modal-body').load('addEditFaculty.io');
		}
		else if(callerBtn.split('-')[0]=='User'){
			$(this).find('.modal-body').load('addEditUser.io');
		}
		
		if(callerBtn.split('-')[1]=='Add'){
			$('#btnAddEditModel').text('Save');
		}
		else if(callerBtn.split('-')[1]=='Edit'){
			$('#btnAddEditModel').text('Edit');
			sessionStorage.setItem("editConfiguration",callerBtn.split('-')[2]);
		}
		
		$('#modelAddEditHeader').text(callerBtn.split('-')[1]+' '+callerBtn.split('-')[0]);
		
	});
	
});

function showActiveConfigureTab(activeTab){

	$('#ulNavTab li').each(function(){
		$(this).removeClass('active');
		if($(this).text()==activeTab){
			$(this).addClass('active');
		}
	});

	$('#divFeedbackSchedule, #divBatch, #divFaculty, #divUser').addClass('hide');
	$('#divNavTabs div').each(function(){
		if($(this).prop('id')=='div'+activeTab.replace(/\s+/g, "")){
			$(this).removeClass('hide');
		}
	});
	
	if($('#divFeedbackSchedule').is(":visible")){
		getFeedbackSchedule();
	}else if($('#divBatch').is(":visible")){
		getBatchDetails();
	}else if($('#divFaculty').is(":visible")){
		getFacultyDetails();
	}else if($('#divUser').is(":visible")){
		getUserDetails();
	}
}


function getFeedbackSchedule(){
	$.ajax({
		url:'getAllFeedbackScheduleList.io',
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function(){
			magicStartBlur();
		},
		success:function(jsonArray){
			var str='<table id="tblFeedbackSchedule" class="table table-condensed table-hover"><thead><tr><th style="width:20px;text-align:center;">#</th>'+
			'<th>Name</th><th style="width:100px;">Status</th><th style="width:100px;text-align:center;">Action</th></tr></thead>'
			
			str+='<tbody>';
			var i=0;
			var feedbackScheduleId=0;
			$.each(jsonArray, function() {
				str+='<tr>';
				$.each(this, function(key, value) {
					if(key=='feedbackScheduleId'){
						str+='<td style="text-align:center;">'+parseInt(++i)+'</td>';
						feedbackScheduleId=value;
					}	
					else
						str+='<td>'+value+'</td>';
				});
				str+='<td style="text-align:center;">'+
						'<span data-toggle="modal" data-target="#modelAddEdit" data-backdrop="static" data-callerbtn="Feedback Schedule-Edit-'+feedbackScheduleId+'">'+
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="left" title="Edit">'+
						'<span class="glyphicon glyphicon-edit"></span></button></span>'+
						
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="right" title="Delete" '+
						'onclick="deleteFeedbackSchedule('+feedbackScheduleId+')"><span class="glyphicon glyphicon-trash"></span></button>'+
					 '</td>';
				str+='</tr>';
			});
			str+='</tbody>';
			
			str+='</table>';
			$('#divFeedbackScheduleList').html(str);
			$('#tblFeedbackSchedule').DataTable();
			$('[data-toggle="tooltip"]').tooltip();
		},
		error:function(){
			magicError('Calling "getAllFeedbackScheduleList.io" ');
		},
		complete:function(){
			magicStopBlur();
		},
	});
}

function deleteFeedbackSchedule(feedbackScheduleId){
	$.confirm({
	    text: "Are you sure you want to delete ?",
	    //title: "Confirmation required",
	    confirm: function() {
	    	var isActive = "false";
	    	$.ajax({
	    		url:'saveOrUpdateFeedbackSchedule.io',
	    		data:{feedbackScheduleId:feedbackScheduleId,isActive:isActive},
	    		type:'POST',
	    		dataType:'text',
	    		cache: false,
	    		beforeSend:function() {
	    			magicStartBlur();
	    		},
	    		success:function(msg) {
	    			$('#modelAddEdit').modal('hide');
	    			getFeedbackSchedule();
	    			magicSuccess(msg);
	    		},
	    		error:function(msg) {
	    			magicError('Calling "saveOrUpdateFeedbackSchedule.io"');
	    		},
	    		complete:function() {
	    			magicStopBlur();
	    		},
	    	});
	    },
	    cancel: function() { /* nothing to do */ },
	    confirmButton: "Yes, I am",
	    cancelButton: "No !",
	    post: false,
	    confirmButtonClass: "btn-danger",
	    cancelButtonClass: "btn-default",
	    dialogClass: "modal-dialog modal-sm"
	});
}

function getBatchDetails() {
	$.ajax({
		url:'getAllBatchList.io',
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function(){
			magicStartBlur();
		},
		success:function(jsonArray){
			var str='<table id="tblBatch" class="table table-condensed table-hover"><thead><tr><th style="width:20px;text-align:center;">#</th>'+
			'<th>Name</th><th style="width:100px;">Status</th><th style="width:100px;text-align:center;">Action</th></tr></thead>'
			
			str+='<tbody>';
			var i=0;
			var batchId=0;
			$.each(jsonArray, function() {
				str+='<tr>';
				$.each(this, function(key, value) {
					if(key=='batchId'){
						str+='<td style="text-align:center;">'+parseInt(++i)+'</td>';
						batchId=value;
					}	
					else
						str+='<td>'+value+'</td>';
				});
				str+='<td style="text-align:center;">'+
						'<span data-toggle="modal" data-target="#modelAddEdit" data-backdrop="static" data-callerbtn="Batch-Edit-'+batchId+'">'+
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="left" title="Edit">'+
						'<span class="glyphicon glyphicon-edit"></span></button></span>'+
						
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="right" title="Delete" '+
						'onclick="deleteBatch('+batchId+')"><span class="glyphicon glyphicon-trash"></span></button>'+
					 '</td>';
				str+='</tr>';
			});
			str+='</tbody>';
			
			str+='</table>';
			$('#divBatchList').html(str);
			$('#tblBatch').DataTable();
			$('[data-toggle="tooltip"]').tooltip();
		},
		error:function(){
			magicError('Calling "getAllBatchList.io" ');
		},
		complete:function(){
			magicStopBlur();
		},
	});
}

function deleteBatch(batchId){
	$.confirm({
	    text: "Are you sure you want to delete ?",
	    //title: "Confirmation required",
	    confirm: function() {
	    	var isActive = "false";
	    	$.ajax({
	    		url:'saveOrUpdateBatch.io',
	    		data:{batchId:batchId,isActive:isActive},
	    		type:'POST',
	    		dataType:'text',
	    		cache: false,
	    		beforeSend:function() {
	    			magicStartBlur();
	    		},
	    		success:function(msg) {
	    			$('#modelAddEdit').modal('hide');
	    			getBatchDetails();
	    			magicSuccess(msg);
	    		},
	    		error:function(msg) {
	    			magicError('Calling "saveOrUpdateBatch.io"');
	    		},
	    		complete:function() {
	    			magicStopBlur();
	    		},
	    	});
	    },
	    cancel: function() { /* nothing to do */ },
	    confirmButton: "Yes, I am",
	    cancelButton: "No !",
	    post: false,
	    confirmButtonClass: "btn-danger",
	    cancelButtonClass: "btn-default",
	    dialogClass: "modal-dialog modal-sm"
	});
}

function getFacultyDetails() {
	$.ajax({
		url:'getAllFacultyList.io',
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function(){
			magicStartBlur();
		},
		success:function(jsonArray){
			var str='<table id="tblFaculty" class="table table-condensed table-hover"><thead><tr><th style="width:20px;text-align:center;">#</th>'+
			'<th>Name</th><th style="width:100px;">Status</th><th style="width:100px;text-align:center;">Action</th></tr></thead>'
			
			str+='<tbody>';
			var i=0;
			var batchId=0;
			$.each(jsonArray, function() {
				str+='<tr>';
				$.each(this, function(key, value) {
					if(key=='facultyId'){
						str+='<td style="text-align:center;">'+parseInt(++i)+'</td>';
						facultyId=value;
					}	
					else
						str+='<td>'+value+'</td>';
				});
				str+='<td style="text-align:center;">'+
						'<span data-toggle="modal" data-target="#modelAddEdit" data-backdrop="static" data-callerbtn="Faculty-Edit-'+facultyId+'">'+
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="left" title="Edit">'+
						'<span class="glyphicon glyphicon-edit"></span></button></span>'+
						
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="right" title="Delete" '+
						'onclick="deleteFaculty('+facultyId+')"><span class="glyphicon glyphicon-trash"></span></button>'+
					 '</td>';
				str+='</tr>';
			});
			str+='</tbody>';
			
			str+='</table>';
			$('#divFacultyList').html(str);
			$('#tblFaculty').DataTable();
			$('[data-toggle="tooltip"]').tooltip();
		},
		error:function(){
			magicError('Calling "getAllFacultyList.io" ');
		},
		complete:function(){
			magicStopBlur();
		},
	});
}

function deleteFaculty(facultyId){
	$.confirm({
	    text: "Are you sure you want to delete ?",
	    //title: "Confirmation required",
	    confirm: function() {
	    	var isActive = "false";
	    	$.ajax({
	    		url:'saveOrUpdateFaculty.io',
	    		data:{facultyId:facultyId,isActive:isActive},
	    		type:'POST',
	    		dataType:'text',
	    		cache: false,
	    		beforeSend:function() {
	    			magicStartBlur();
	    		},
	    		success:function(msg) {
	    			$('#modelAddEdit').modal('hide');
	    			getFacultyDetails();
	    			magicSuccess(msg);
	    		},
	    		error:function(msg) {
	    			magicError('Calling "saveOrUpdateFaculty.io"');
	    		},
	    		complete:function() {
	    			magicStopBlur();
	    		},
	    	});
	    },
	    cancel: function() { /* nothing to do */ },
	    confirmButton: "Yes, I am",
	    cancelButton: "No !",
	    post: false,
	    confirmButtonClass: "btn-danger",
	    cancelButtonClass: "btn-default",
	    dialogClass: "modal-dialog modal-sm"
	});
}

function getUserDetails() {
	$.ajax({
		url:'getAllUserList.io',
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function(){
			magicStartBlur();
		},
		success:function(jsonArray){
			var str='<table id="tblUser" class="table table-condensed table-hover"><thead><tr><th style="width:20px;text-align:center;">#</th>'+
			'<th>Name</th><th>Login Id</th><th style="width:100px;">Status</th><th style="width:100px;text-align:center;">Action</th></tr></thead>'
			
			str+='<tbody>';
			var i=0;
			var batchId=0;
			$.each(jsonArray, function() {
				str+='<tr>';
				$.each(this, function(key, value) {
					if(key=='userId'){
						str+='<td style="text-align:center;">'+parseInt(++i)+'</td>';
						userId=value;
					}	
					else
						str+='<td>'+value+'</td>';
				});
				str+='<td style="text-align:center;">'+
						'<span data-toggle="modal" data-target="#modelAddEdit" data-backdrop="static" data-callerbtn="User-Edit-'+userId+'">'+
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="left" title="Edit">'+
						'<span class="glyphicon glyphicon-edit"></span></button></span>'+
						
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Reset Password" '+
						'onclick="resetPassword('+userId+')"><span class="glyphicon glyphicon-repeat"></span></button>'+
						
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="right" title="Delete" '+
						'onclick="deleteUser('+userId+')"><span class="glyphicon glyphicon-trash"></span></button>'+
					 '</td>';
				str+='</tr>';
			});
			str+='</tbody>';
			
			str+='</table>';
			$('#divUserList').html(str);
			$('#tblUser').DataTable();
			$('[data-toggle="tooltip"]').tooltip();
		},
		error:function(){
			magicError('Calling "getAllUserList.io" ');
		},
		complete:function(){
			magicStopBlur();
		},
	});
}

function deleteUser(userId){
	$.confirm({
	    text: "Are you sure you want to delete ?",
	    //title: "Confirmation required",
	    confirm: function() {
	    	var isActive = "false";
	    	var action = "Edit";
	    	$.ajax({
	    		url:'saveOrUpdateUser.io',
	    		data:{userId:userId,isActive:isActive,action:action},
	    		type:'POST',
	    		dataType:'text',
	    		cache: false,
	    		beforeSend:function() {
	    			magicStartBlur();
	    		},
	    		success:function(msg) {
	    			$('#modelAddEdit').modal('hide');
	    			getUserDetails()
	    			magicSuccess(msg);
	    		},
	    		error:function(msg) {
	    			magicError('Calling "saveOrUpdateUser.io"');
	    		},
	    		complete:function() {
	    			magicStopBlur();
	    		},
	    	});
	    },
	    cancel: function() { /* nothing to do */ },
	    confirmButton: "Yes, I am",
	    cancelButton: "No !",
	    post: false,
	    confirmButtonClass: "btn-danger",
	    cancelButtonClass: "btn-default",
	    dialogClass: "modal-dialog modal-sm"
	});
}

function resetPassword(userId){
	$.confirm({
	    text: "Are you sure you want to reset password ?",
	    //title: "Confirmation required",
	    confirm: function() {
	    	var action = "Save";
	    	$.ajax({
	    		url:'saveOrUpdateUser.io',
	    		data:{userId:userId,action:action},
	    		type:'POST',
	    		dataType:'text',
	    		cache: false,
	    		beforeSend:function() {
	    			magicStartBlur();
	    		},
	    		success:function(msg) {
	    			$('#modelAddEdit').modal('hide');
	    			getUserDetails()
	    			magicSuccess(msg);
	    		},
	    		error:function(msg) {
	    			magicError('Calling "saveOrUpdateUser.io"');
	    		},
	    		complete:function() {
	    			magicStopBlur();
	    		},
	    	});
	    },
	    cancel: function() { /* nothing to do */ },
	    confirmButton: "Yes, I am",
	    cancelButton: "No !",
	    post: false,
	    confirmButtonClass: "btn-danger",
	    cancelButtonClass: "btn-default",
	    dialogClass: "modal-dialog modal-sm"
	});
}
