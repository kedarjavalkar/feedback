$('document').ready(function() {

	$('#btnAddEditModel').bind('click', function() {
		$('#btnBatchAddEdit').click();
	});
	
	$('#formBatchAddEdit').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			// do something when error
		} else {
			e.preventDefault(); //preventing form submit
			saveOrUpdateBatch();
		}
	});
	
	if(sessionStorage.getItem("editConfiguration")!=null){
		editBatchDetails(sessionStorage.getItem("editConfiguration"));
		sessionStorage.removeItem("editConfiguration")
	}
	
});

function editBatchDetails(batchId){
	$('#formBatchAddEdit').data('batchId',batchId);

	$.ajax({
		url:'editBatch.io',
		data:{batchId:batchId},
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function() {
			magicStartBlur();
		},
		success:function(json) {
			$('#inputName').val(json['name']);
			$('#isActive').prop('checked', json['isActive']=="Active"?true:false);
		},
		error:function() {
			magicError('Calling "editBatch.io" ');
		},
		complete:function() {
			magicStopBlur();
		},
	});
}

function saveOrUpdateBatch() {

	var batchId = $('#formBatchAddEdit').data('batchId');
	var name = $('#inputName').val().trim();
	var isActive = "false";
	if ($('#isActive').is(":checked"))
		isActive = "true";
	
	$.ajax({
		url:'saveOrUpdateBatch.io',
		data:{batchId:batchId,name:name,isActive:isActive},
		type:'POST',
		dataType:'text',
		cache: false,
		beforeSend:function() {
			magicStartBlur();
		},
		success:function(msg) {
			$('#modelAddEdit').modal('hide');
			getBatchDetails();
			magicSuccess(msg);
		},
		error:function(msg) {
			magicError('Calling "saveOrUpdateBatch.io"');
		},
		complete:function() {
			magicStopBlur();
		},
	});
}

