$('document').ready(function() {

	$('#btnAddEditModel').bind('click', function() {
		$('#btnFacultyAddEdit').click();
	});
	
	$('#formFacultyAddEdit').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			// do something when error
		} else {
			e.preventDefault(); //preventing form submit
			saveOrUpdateFaculty();
		}
	});
	
	if(sessionStorage.getItem("editConfiguration")!=null){
		editFacultyDetails(sessionStorage.getItem("editConfiguration"));
		sessionStorage.removeItem("editConfiguration")
	}
	
});

function editFacultyDetails(facultyId){
	$('#formFacultyAddEdit').data('facultyId',facultyId);

	$.ajax({
		url:'editFaculty.io',
		data:{facultyId:facultyId},
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function() {
			magicStartBlur();
		},
		success:function(json) {
			$('#inputFirstName').val(json['firstName']);
			$('#inputMiddleName').val(json['middleName']);
			$('#inputLastName').val(json['lastName']);
			$('#isActive').prop('checked', json['isActive']=="Active"?true:false);
		},
		error:function() {
			magicError('Calling "editFaculty.io" ');
		},
		complete:function() {
			magicStopBlur();
		},
	});
}

function saveOrUpdateFaculty() {

	var facultyId = $('#formFacultyAddEdit').data('facultyId');
	var firstName = $('#inputFirstName').val().trim();
	var middleName = $('#inputMiddleName').val().trim();
	var lastName = $('#inputLastName').val().trim();
	var isActive = "false";
	if ($('#isActive').is(":checked"))
		isActive = "true";
	
	$.ajax({
		url:'saveOrUpdateFaculty.io',
		data:{facultyId:facultyId,firstName:firstName,middleName:middleName,lastName:lastName,isActive:isActive},
		type:'POST',
		dataType:'text',
		cache: false,
		beforeSend:function() {
			magicStartBlur();
		},
		success:function(msg) {
			$('#modelAddEdit').modal('hide');
			getFacultyDetails();
			magicSuccess(msg);
		},
		error:function(msg) {
			magicError('Calling "saveOrUpdateFaculty.io"');
		},
		complete:function() {
			magicStopBlur();
		},
	});
}

