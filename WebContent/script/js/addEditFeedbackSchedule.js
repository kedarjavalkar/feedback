
$('document').ready(function() {

    $('#inputSchedule').datepicker({
        format: "MM yyyy",
        minViewMode: 1,
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        toggleActive: true
    });
	
	$('#btnAddEditModel').bind('click', function() {
		$('#btnFeedbackScheduleAddEdit').click();
	});
	
	$('#formFeedbackScheduleAddEdit').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			// do something when error
		} else {
			e.preventDefault(); //preventing form submit
			saveOrUpdateFeedbackSchedule();
		}
	});
	
	if(sessionStorage.getItem("editConfiguration")!=null){
		editFeedbackScheduleDetails(sessionStorage.getItem("editConfiguration"));
		sessionStorage.removeItem("editConfiguration")
	}
	
});

function editFeedbackScheduleDetails(feedbackScheduleId){
	$('#formFeedbackScheduleAddEdit').data('feedbackScheduleId',feedbackScheduleId);

	$.ajax({
		url:'editFeedbackSchedule.io',
		data:{feedbackScheduleId:feedbackScheduleId},
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function() {
			magicStartBlur();
		},
		success:function(json) {
			$('#inputSchedule').val(json['feedbackSchedule']);
			$('#isActive').prop('checked', json['isActive']=="Active"?true:false);
		},
		error:function() {
			magicError('Calling "editFeedbackSchedule.io" ');
		},
		complete:function() {
			magicStopBlur();
		},
	});
}

function saveOrUpdateFeedbackSchedule() {

	var feedbackScheduleId = $('#formFeedbackScheduleAddEdit').data('feedbackScheduleId');
	var schedule = $('#inputSchedule').val().trim();
	var isActive = "false";
	if ($('#isActive').is(":checked"))
		isActive = "true";
	
	$.ajax({
		url:'saveOrUpdateFeedbackSchedule.io',
		data:{feedbackScheduleId:feedbackScheduleId,schedule:schedule,isActive:isActive},
		type:'POST',
		dataType:'text',
		cache: false,
		beforeSend:function() {
			magicStartBlur();
		},
		success:function(msg) {
			$('#modelAddEdit').modal('hide');
			getFeedbackSchedule();
			magicSuccess(msg);
		},
		error:function(msg) {
			magicError('Calling "saveOrUpdateFeedbackSchedule.io"');
		},
		complete:function() {
			magicStopBlur();
		},
	});
}

