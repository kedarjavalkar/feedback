$('document').ready(function() {

	$('#btnAddEditModel').bind('click', function() {
		$('#btnUserAddEdit').click();
	});
	
	$('#formUserAddEdit').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			// do something when error
		} else {
			e.preventDefault(); //preventing form submit
			saveOrUpdateUser();
		}
	});
	
	if(sessionStorage.getItem("editConfiguration")!=null){
		editUserDetails(sessionStorage.getItem("editConfiguration"));
		sessionStorage.removeItem("editConfiguration")
	}
	
});

function editUserDetails(userId){
	$('#formUserAddEdit').data('userId',userId);

	$.ajax({
		url:'editUser.io',
		data:{userId:userId},
		type:'GET',
		dataType:'json',
		cache: false,
		beforeSend:function() {
			magicStartBlur();
		},
		success:function(json) {
			$('#inputFirstName').val(json['firstName']);
			$('#inputMiddleName').val(json['middleName']);
			$('#inputLastName').val(json['lastName']);
			$('#inputLoginId').val(json['loginId']);
			$('#isActive').prop('checked', json['isActive']=="Active"?true:false);
		},
		error:function() {
			magicError('Calling "editUser.io" ');
		},
		complete:function() {
			magicStopBlur();
		},
	});
}

function saveOrUpdateUser() {
	
	var userId = $('#formUserAddEdit').data('userId');
	var firstName = $('#inputFirstName').val().trim();
	var middleName = $('#inputMiddleName').val().trim();
	var lastName = $('#inputLastName').val().trim();
	var loginId = $('#inputLoginId').val().trim();
	var isActive = "false";
	if ($('#isActive').is(":checked"))
		isActive = "true";
	var action = $('#btnAddEditModel').text();
	
	$.ajax({
		url:'saveOrUpdateUser.io',
		data:{userId:userId,firstName:firstName,middleName:middleName,lastName:lastName,loginId:loginId,isActive:isActive,action:action},
		type:'POST',
		dataType:'text',
		cache: false,
		beforeSend:function() {
			magicStartBlur();
		},
		success:function(msg) {
			if (msg.toLowerCase().indexOf("already associated") >= 0)
				magicError(msg);
			else{
				$('#modelAddEdit').modal('hide');
				getUserDetails();
				magicSuccess(msg);
			}
		},
		error:function(msg) {
			magicError('Calling "saveOrUpdateUser.io"');
		},
		complete:function() {
			magicStopBlur();
		},
	});
}

