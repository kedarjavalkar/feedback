package io.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class CommonUtils {
	
	private final static String PATTERN = "yyyy-MM-dd_HH:mm:ss";
	
	private final static TimeZone GMT_TIME_ZONE = TimeZone.getTimeZone("GMT");
	private final static SimpleDateFormat GMT_FORMATTER = new SimpleDateFormat(PATTERN);
	
	private final static TimeZone UAE_TIME_ZONE = TimeZone.getTimeZone("GMT+04:00");
	private final static SimpleDateFormat UAE_FORMATTER = new SimpleDateFormat(PATTERN);
	
	private final static TimeZone IST_TIME_ZONE = TimeZone.getTimeZone("GMT+05:30");
	private final static SimpleDateFormat IST_FORMATTER = new SimpleDateFormat(PATTERN);
	
	private final static String MONTH_PATTERN = "MMMM yyyy";
	private final static SimpleDateFormat MONTH_FORMATTER = new SimpleDateFormat(MONTH_PATTERN);
	
	static {
		GMT_FORMATTER.setTimeZone(GMT_TIME_ZONE);
		UAE_FORMATTER.setTimeZone(UAE_TIME_ZONE);
		IST_FORMATTER.setTimeZone(IST_TIME_ZONE);
	}
	
	public static String getGMTTimestamp(Date date) {
		return GMT_FORMATTER.format(date);
	}
	
	public static String getUAETimestamp(Date date) {
		return UAE_FORMATTER.format(date);
	}
	
	public static String getISTTimestamp(Date date) {
		return IST_FORMATTER.format(date);
	}
	
	
	public static String getMonthYearTimestamp(Date date){
		return MONTH_FORMATTER.format(date);
	}
	
	public static Date getMonthYear(String date) throws ParseException{
		return MONTH_FORMATTER.parse(date);
	}
	
	
	

}
