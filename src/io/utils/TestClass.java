package io.utils;

import java.text.ParseException;
import java.util.Date;

public class TestClass {
	
	public static void main(String args[]) throws ParseException {
		
		System.out.println(new Date());
		System.out.println("GMT: "+CommonUtils.getGMTTimestamp(new Date()));
		System.out.println("UAE: "+CommonUtils.getUAETimestamp(new Date()));
		System.out.println("IST: "+CommonUtils.getISTTimestamp(new Date()));
		
	}
	
}
