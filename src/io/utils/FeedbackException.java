package io.utils;

public class FeedbackException extends Exception {

	private static final long serialVersionUID = -3318591841491937156L;

	private String message = null;
	
	public FeedbackException() {
		super();
	}
	
	public FeedbackException(String message) {
		super();
		this.message = message;
	}
	
	public FeedbackException(Throwable cause) {
		super(cause);
	}
	
	@Override
	public String toString() {
		return message;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
}
