package io.bi.facultyFeedback;

import java.util.List;

import io.entity.FacultyFeedback;

public interface FacultyFeedbackManager {

	public FacultyFeedback save(FacultyFeedback facultyFeedback);

	public List<FacultyFeedback> getAll();
}
