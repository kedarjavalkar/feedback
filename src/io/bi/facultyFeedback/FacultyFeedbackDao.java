package io.bi.facultyFeedback;

import java.util.List;

import io.entity.FacultyFeedback;

public interface FacultyFeedbackDao {

	public FacultyFeedback save(FacultyFeedback facultyFeedback);
	public List<FacultyFeedback> getAll();

}
