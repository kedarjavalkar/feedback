package io.bi.facultyFeedback;

import io.entity.FacultyFeedback;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("FacultyFeedbackDao")
public class FacultyFeedbackDaoImpl implements FacultyFeedbackDao {

	@Autowired
	SessionFactory sessionFactory;


	@Override
	public FacultyFeedback save(FacultyFeedback facultyFeedback) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(facultyFeedback);
		tx.commit();
		session.close();

		return facultyFeedback;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<FacultyFeedback> getAll() {
		List<FacultyFeedback> facultyFeedbackList = null;

		Session session = sessionFactory.openSession();
		facultyFeedbackList = session.createCriteria(FacultyFeedback.class)
				.addOrder(Order.asc("id"))
				.list();
		session.close();

		if (facultyFeedbackList.size() == 0) {
			return new ArrayList<FacultyFeedback>();
		}

		return facultyFeedbackList;
	}


}
