package io.bi.facultyFeedback;

import java.util.List;

import io.entity.FacultyFeedback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("FacultyFeedbackManager")
public class FacultyFeedbackManagerImpl implements FacultyFeedbackManager {

	@Autowired
	FacultyFeedbackDao feedbackDao;

	@Override
	public FacultyFeedback save(FacultyFeedback facultyFeedback) {
		return feedbackDao.save(facultyFeedback);
	}

	@Override
	public List<FacultyFeedback> getAll() {
		return feedbackDao.getAll();
	}
	
	
	
}
