package io.bi.user;

import java.util.List;

import io.entity.User;
import io.utils.FeedbackException;


public interface UserDao {

	public User checkUserLogin(String login, String password);
	public List<User> getAll();
	public User getById(Integer userId);
	public User save(User user) throws FeedbackException;
	
}
