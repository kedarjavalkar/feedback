package io.bi.user;

import java.util.List;

import io.entity.User;
import io.utils.FeedbackException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("UserManager")
public class UserManagerImpl implements UserManager {

	@Autowired
	UserDao userDao;
	
	@Override
	public User checkUserLogin(String login, String password){
		return userDao.checkUserLogin(login, password);
	}

	@Override
	public List<User> getAll() {
		return userDao.getAll();
	}

	@Override
	public User getById(Integer userId) {
		return userDao.getById(userId);
	}

	@Override
	public User save(User user) throws FeedbackException {
		return userDao.save(user);
	}
	
}
