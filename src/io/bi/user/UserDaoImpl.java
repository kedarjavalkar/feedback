package io.bi.user;

import io.entity.User;
import io.utils.FeedbackException;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Transactional
@Repository("UserDao")
public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public User checkUserLogin(String login, String password) {
		List<User> userList = null;
		
		Session session = sessionFactory.openSession();
		userList = session.createCriteria(User.class)
				.add(Restrictions.eq("loginName", login))
				.add(Restrictions.eq("password", password))
				.add(Restrictions.eq("isActive", true))
				.list();

		session.close();

		if (userList.size() == 0) {
			return null;
		}
		
		User user = userList.get(0);
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAll() {
		List<User> userList = null;
		
		Session session = sessionFactory.openSession();
		userList = session.createCriteria(User.class)
				.list();

		session.close();

		if (userList.size() == 0) {
			return new ArrayList<User>();
		}
		
		return userList;
	}
	
	
	@Override
	public User getById(Integer userId) {
		User obj = null;
		
		Session session = sessionFactory.openSession();
		obj = (User) session.get(User.class, userId);
		session.close();
		
		return obj;
	}
	
	@Override
	public User save(User user) throws FeedbackException {
		
		if(user.getId()==null){
			if (!checkUniqueLogin(user.getLoginName()))
				throw new FeedbackException("Login id already associated with another user...");
		}

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(user);
		tx.commit();
		session.close();
		
		return user;
	}

	@SuppressWarnings("unchecked")
	private boolean checkUniqueLogin(String loginName) {
		List<User> list = null;
		
		Session session = sessionFactory.openSession();
		list = session.createCriteria(User.class)
				.add(Restrictions.eq("loginName", loginName))
				.list();
		session.close();
		
		if(list.isEmpty())
			return true;
		
		return false;
	}
	
	
	
	

}
