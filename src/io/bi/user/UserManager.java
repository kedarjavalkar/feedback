package io.bi.user;

import io.entity.User;
import io.utils.FeedbackException;

import java.util.List;

public interface UserManager {
	
	public User checkUserLogin(String login, String password);
	public List<User> getAll();
	public User getById(Integer userId);
	public User save(User user) throws FeedbackException ;

}
