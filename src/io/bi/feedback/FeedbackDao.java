package io.bi.feedback;

import io.entity.Feedback;

import java.util.List;

public interface FeedbackDao {

	public List<Feedback> getAllActive();

}
