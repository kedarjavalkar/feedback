package io.bi.feedback;

import io.entity.Feedback;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("FeedbackDao")
public class FeedbackDaoImpl implements FeedbackDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Feedback> getAllActive() {
		List<Feedback> list = null;
		
		Session session = sessionFactory.openSession();
		list = session.createCriteria(Feedback.class)
				.add(Restrictions.eq("isActive", true))
				.list();
		session.close();
		
		if (list.size() == 0) {
			return new ArrayList<Feedback>();
		}
		return list;
	}

	
	
}
