package io.bi.feedback;

import io.entity.Feedback;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("FeedbackManager")
public class FeedbackManagerImpl implements FeedbackManager {

	@Autowired
	FeedbackDao feedbackDao;
	
	@Override
	public List<Feedback> getAllActive() {
		return feedbackDao.getAllActive();
	}

	
	
}
