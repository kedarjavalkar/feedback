package io.bi.feedbackSchedule;

import io.entity.FeedbackSchedule;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("FeedbackScheduleManager")
public class FeedbackScheduleManagerImpl implements FeedbackScheduleManager {
	
	@Autowired
	FeedbackScheduleDao feedbackScheduleDao;
	
	@Override
	public List<FeedbackSchedule> getAllActive() {
		return feedbackScheduleDao.getAllActive();
	}

	@Override
	public FeedbackSchedule save(FeedbackSchedule feedbackSchedule) {
		return feedbackScheduleDao.save(feedbackSchedule);
	}

	@Override
	public List<FeedbackSchedule> getAll() {
		return feedbackScheduleDao.getAll();
	}

	@Override
	public FeedbackSchedule getById(Integer feedbackScheduleId) {
		return feedbackScheduleDao.getById(feedbackScheduleId);
	}

}
