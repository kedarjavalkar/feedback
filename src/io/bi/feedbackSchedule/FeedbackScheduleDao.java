package io.bi.feedbackSchedule;

import io.entity.FeedbackSchedule;

import java.util.List;

public interface FeedbackScheduleDao {

	public List<FeedbackSchedule> getAllActive();

	public List<FeedbackSchedule> getAll();
	
	public FeedbackSchedule save(FeedbackSchedule feedbackSchedule);

	public FeedbackSchedule getById(Integer feedbackScheduleId);
	

}
