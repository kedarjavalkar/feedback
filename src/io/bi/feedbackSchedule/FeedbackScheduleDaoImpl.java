package io.bi.feedbackSchedule;

import io.entity.FeedbackSchedule;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Transactional
@Repository("FeedbackScheduleDao")
public class FeedbackScheduleDaoImpl implements FeedbackScheduleDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FeedbackSchedule> getAllActive() {
		List<FeedbackSchedule> list = null;
		
		Session session = sessionFactory.openSession();
		list = session.createCriteria(FeedbackSchedule.class)
				.add(Restrictions.eq("isActive", true))
				.list();
		session.close();
		
		if (list.size() == 0) {
			return new ArrayList<FeedbackSchedule>();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FeedbackSchedule> getAll() {
		List<FeedbackSchedule> list = null;
		
		Session session = sessionFactory.openSession();
		list = session.createCriteria(FeedbackSchedule.class)
				.list();
		session.close();
		
		if (list.size() == 0) {
			return new ArrayList<FeedbackSchedule>();
		}
		return list;
	}

	@Override
	public FeedbackSchedule save(FeedbackSchedule feedbackSchedule) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(feedbackSchedule);
		tx.commit();
		session.close();
		
		return feedbackSchedule;
	}

	@Override
	public FeedbackSchedule getById(Integer feedbackScheduleId) {
		FeedbackSchedule obj = null;
		
		Session session = sessionFactory.openSession();
		obj = (FeedbackSchedule) session.get(FeedbackSchedule.class, feedbackScheduleId);
		session.close();
		
		return obj;
	}

}
