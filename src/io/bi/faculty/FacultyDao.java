package io.bi.faculty;

import io.entity.Faculty;

import java.util.List;

public interface FacultyDao {

	public List<Faculty> getAllActive();
	public List<Faculty> getAll();
	public Faculty getById(Integer facultyId);
	public Faculty save(Faculty faculty);

}
