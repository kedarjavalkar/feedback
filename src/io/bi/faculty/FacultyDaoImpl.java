package io.bi.faculty;

import io.entity.Faculty;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Transactional
@Repository("FacultyDao")
public class FacultyDaoImpl implements FacultyDao {
	
	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Faculty> getAllActive() {
		List<Faculty> list = null;
		
		Session session = sessionFactory.openSession();
		list = session.createCriteria(Faculty.class)
				.add(Restrictions.eq("isActive", true))
				.list();
		session.close();
		
		if (list.size() == 0) {
			return new ArrayList<Faculty>();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Faculty> getAll() {
		List<Faculty> list = null;
		
		Session session = sessionFactory.openSession();
		list = session.createCriteria(Faculty.class)
				.list();
		session.close();
		
		if (list.size() == 0) {
			return new ArrayList<Faculty>();
		}
		return list;
	}

	@Override
	public Faculty getById(Integer facultyId) {
		Faculty obj = null;
		
		Session session = sessionFactory.openSession();
		obj = (Faculty) session.get(Faculty.class, facultyId);
		session.close();
		
		return obj;
	}

	@Override
	public Faculty save(Faculty faculty) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(faculty);
		tx.commit();
		session.close();
		
		return faculty;
	}
	
}
