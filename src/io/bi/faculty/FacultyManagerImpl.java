package io.bi.faculty;

import io.entity.Faculty;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("FacultyManager")
public class FacultyManagerImpl implements FacultyManager {

	@Autowired
	FacultyDao facultyDao;
	
	@Override
	public List<Faculty> getAllActive() {
		return facultyDao.getAllActive();
	}

	@Override
	public List<Faculty> getAll() {
		return facultyDao.getAll();
	}

	@Override
	public Faculty getById(Integer facultyId) {
		return facultyDao.getById(facultyId);
	}

	@Override
	public Faculty save(Faculty faculty) {
		return facultyDao.save(faculty);
	}

}
