package io.bi.batch;

import io.entity.Batch;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("BatchDao")
public class BatchDaoImpl implements BatchDao {

	@Autowired
	SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Batch> getAllActive() {
		List<Batch> list = null;
		
		Session session = sessionFactory.openSession();
		list = session.createCriteria(Batch.class)
				.add(Restrictions.eq("isActive", true))
				.list();
		session.close();
		
		if (list.size() == 0) {
			return new ArrayList<Batch>();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Batch> getAll() {
		List<Batch> list = null;
		
		Session session = sessionFactory.openSession();
		list = session.createCriteria(Batch.class)
				.list();
		session.close();
		
		if (list.size() == 0) {
			return new ArrayList<Batch>();
		}
		return list;
	}

	
	@Override
	public Batch getById(Integer batchId) {
		Batch obj = null;
		
		Session session = sessionFactory.openSession();
		obj = (Batch) session.get(Batch.class, batchId);
		session.close();
		
		return obj;
	}

	@Override
	public Batch save(Batch batch) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(batch);
		tx.commit();
		session.close();
		
		return batch;
	}

}
