package io.bi.batch;

import io.entity.Batch;

import java.util.List;

public interface BatchManager {

	public List<Batch> getAllActive();
	public List<Batch> getAll();
	public Batch getById(Integer batchId);
	public Batch save(Batch batch);
	
}
