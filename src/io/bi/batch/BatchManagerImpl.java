package io.bi.batch;

import io.entity.Batch;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("BatchManager")
public class BatchManagerImpl implements BatchManager {

	@Autowired
	BatchDao batchDao;
	
	@Override
	public List<Batch> getAllActive() {
		return batchDao.getAllActive();
	}

	@Override
	public List<Batch> getAll() {
		return batchDao.getAll();
	}

	@Override
	public Batch getById(Integer batchId) {
		return batchDao.getById(batchId);
	}

	@Override
	public Batch save(Batch batch) {
		return batchDao.save(batch);
	}

}
