package io.controller;

import io.bi.feedback.FeedbackManager;
import io.entity.Feedback;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Controller
public class FeedbackController {

	@Autowired
	FeedbackManager feedbackManager;
	
	@RequestMapping(value="getfeedbackCommentList.io",method=RequestMethod.GET)
	@ResponseBody String getfeedbackCommentList(HttpServletRequest request) {
		List<Feedback> list = feedbackManager.getAllActive();

		JsonArray jsonArray = new JsonArray();
		for (Feedback feedback : list) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("feedbackId", feedback.getId().toString());
			jsonObject.addProperty("comment", feedback.getComment());
			jsonArray.add(jsonObject);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
}
