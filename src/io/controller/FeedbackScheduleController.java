package io.controller;

import io.bi.feedbackSchedule.FeedbackScheduleManager;
import io.entity.FeedbackSchedule;
import io.utils.CommonUtils;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Controller
public class FeedbackScheduleController {

	@Autowired
	private  FeedbackScheduleManager feedbackScheduleManager;
	
	@RequestMapping(value="getFeedbackScheduleList.io",method=RequestMethod.GET)
	@ResponseBody String getFeedbackScheduleList(HttpServletRequest request) {
		List<FeedbackSchedule> list = feedbackScheduleManager.getAllActive();

		JsonArray jsonArray = new JsonArray();
		for (FeedbackSchedule feedbackSchedule : list) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("feedbackScheduleId", feedbackSchedule.getId().toString());
			jsonObject.addProperty("feedbackSchedule", CommonUtils.getMonthYearTimestamp(feedbackSchedule.getSchedule()));
			jsonArray.add(jsonObject);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="getAllFeedbackScheduleList.io",method=RequestMethod.GET)
	@ResponseBody String getAllFeedbackScheduleList(HttpServletRequest request) {
		List<FeedbackSchedule> list = feedbackScheduleManager.getAll();

		JsonArray jsonArray = new JsonArray();
		for (FeedbackSchedule feedbackSchedule : list) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("feedbackScheduleId", feedbackSchedule.getId().toString());
			jsonObject.addProperty("feedbackSchedule", CommonUtils.getMonthYearTimestamp(feedbackSchedule.getSchedule()));
			jsonObject.addProperty("isActive", feedbackSchedule.getIsActive() ? "Active" : "Inactive");
			jsonArray.add(jsonObject);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="editFeedbackSchedule.io",method=RequestMethod.GET)
	@ResponseBody String editFeedbackSchedule(HttpServletRequest request) {

		String feedbackScheduleId = request.getParameter("feedbackScheduleId");
		
		FeedbackSchedule feedbackSchedule = feedbackScheduleManager.getById(Integer.parseInt(feedbackScheduleId));
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("feedbackScheduleId", feedbackSchedule.getId().toString());
		jsonObject.addProperty("feedbackSchedule", CommonUtils.getMonthYearTimestamp(feedbackSchedule.getSchedule()));
		jsonObject.addProperty("isActive", feedbackSchedule.getIsActive() ? "Active" : "Inactive");

		return new Gson().toJson(jsonObject);
	}
	
	
	
	@RequestMapping(value="saveOrUpdateFeedbackSchedule.io",method=RequestMethod.POST)
	@ResponseBody String saveOrUpdateFeedbackSchedule(HttpServletRequest request) throws ParseException {

		String feedbackScheduleId = request.getParameter("feedbackScheduleId");
		String schedule = request.getParameter("schedule");
		String isActive = request.getParameter("isActive");
		
		FeedbackSchedule feedbackSchedule;
		
		if (feedbackScheduleId != null)
			feedbackSchedule = feedbackScheduleManager.getById(Integer.parseInt(feedbackScheduleId));
		else
			feedbackSchedule = new FeedbackSchedule();
			
		if (schedule != null)
			feedbackSchedule.setSchedule(CommonUtils.getMonthYear(schedule));
		feedbackSchedule.setIsActive(isActive.equalsIgnoreCase("true") ? true : false);
		
		feedbackScheduleManager.save(feedbackSchedule);
		
		return "Feedback schedule change accepted";
		
	}
	
	
}
