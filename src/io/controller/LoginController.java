package io.controller;

import io.bi.user.UserManager;
import io.entity.User;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

	private static final Integer SESSION_TIMEOUT = 15 * 60;
	
	@Autowired
	private UserManager userManager;
	
	@RequestMapping(value="userLogin.io",method=RequestMethod.POST)
	@ResponseBody String loginUser(HttpServletRequest request) {
		String login = request.getParameter("login");
		String password = request.getParameter("password");

		User user = userManager.checkUserLogin(login, password);
		
		if(user != null){
			request.getSession().setAttribute("mecca-feedback-userId", user.getId());
			request.getSession().setAttribute("mecca-feedback-key", login);
			request.getSession().setAttribute("mecca-feedback-pass", password);
			request.getSession().setAttribute("mecca-feedback-user", user.getFirstName() + " " + user.getLastName());
			setSessionRoles(request, user.getRole().getName());
			request.getSession().setMaxInactiveInterval(SESSION_TIMEOUT);
			return "Success";
		}
		
		removeSessionRoles(request);
		request.getSession().invalidate();
		return "Failure";
	}
	
	@RequestMapping(value="authenticate.io",method=RequestMethod.POST)
	@ResponseBody String validateUser(HttpServletRequest request){
		
		Object login = request.getSession().getAttribute("mecca-feedback-key");
		Object password = request.getSession().getAttribute("mecca-feedback-pass");
		
		if (login != null && password != null) {
			if(userManager.checkUserLogin(login.toString(), password.toString()) != null){
				return "Success";
			}
		}
		
		request.getSession().invalidate();
		removeSessionRoles(request);
		return "Failure";
	}
	
	@RequestMapping(value="logout.io",method=RequestMethod.POST)
	@ResponseBody String logout(HttpServletRequest request){
		removeSessionRoles(request);
		request.getSession().invalidate();
		return "Success";
	}
	
	private void removeSessionRoles(HttpServletRequest request) {
		request.getSession().removeAttribute("mecca-feedback-userId");
		request.getSession().removeAttribute("mecca-feedback-key");
		request.getSession().removeAttribute("mecca-feedback-pass");
		request.getSession().removeAttribute("mecca-feedback-user");
		request.getSession().removeAttribute("mecca-feedback-role-config");
		request.getSession().removeAttribute("mecca-feedback-role-report");
	}

	private void setSessionRoles(HttpServletRequest request, String role) {
		switch (role) {
		case "Admin":
			request.getSession().setAttribute("mecca-feedback-role-config", true);
			request.getSession().setAttribute("mecca-feedback-role-report", true);
			break;
		
		case "Faculty":
			request.getSession().setAttribute("mecca-feedback-role-config", null);
			request.getSession().setAttribute("mecca-feedback-role-report", true);
			break;
			
		default:
			break;
		}
	}
	
	
}
