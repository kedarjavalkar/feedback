package io.controller;

import io.bi.facultyFeedback.FacultyFeedbackManager;
import io.entity.FacultyFeedback;
import io.entity.User;
import io.utils.CommonUtils;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Controller
public class FacultyFeedbackController {

	@Autowired
	FacultyFeedbackManager facultyFeedbackManager;
	
	@RequestMapping(value="saveFacultyFeedback.io",method=RequestMethod.POST)
	@ResponseBody String saveOrUpdateFaculty(HttpServletRequest request) {
		Date currentDate = new Date();
		Object userId = request.getSession().getAttribute("mecca-feedback-userId");
		User userObj = new User();
		userObj.setId(Integer.parseInt(userId.toString()));
		
		String[] facultyFeedback = request.getParameterValues("facultyFeedback[]");
		for (String str : facultyFeedback) {
			FacultyFeedback facultyFeedbackObj = new Gson().fromJson(str, FacultyFeedback.class);
			facultyFeedbackObj.setCreatedDate(currentDate);
			facultyFeedbackObj.setUser(userObj);
			facultyFeedbackManager.save(facultyFeedbackObj);
		}
		return "Feedback saved";
	}
	
	@RequestMapping(value="rawExcelReport.io",method=RequestMethod.GET)
	@ResponseBody String rawExcelReport(HttpServletRequest request) {
		List<FacultyFeedback> facultyFeedbackList = facultyFeedbackManager.getAll();
		
		JsonArray jsonArray = new JsonArray();
		for (FacultyFeedback facultyFeedback : facultyFeedbackList) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("facultyfeedbackId", facultyFeedback.getId().toString());
			jsonObject.addProperty("studentName", facultyFeedback.getStudentName());
			jsonObject.addProperty("batch", facultyFeedback.getBatch().getName());
			String feedbackSchedule = CommonUtils.getMonthYearTimestamp(facultyFeedback.getFeedbackSchedule().getSchedule());
			jsonObject.addProperty("feedbackSchedule", feedbackSchedule);
			String fullName = facultyFeedback.getFaculty().getFirstName() + 
					(facultyFeedback.getFaculty().getMiddleName()==null?"":" "+facultyFeedback.getFaculty().getMiddleName()) + 
					" "+facultyFeedback.getFaculty().getLastName();
			jsonObject.addProperty("faculty", fullName);
			jsonObject.addProperty("feedback", facultyFeedback.getFeedback().getComment());
			jsonObject.addProperty("physics", facultyFeedback.getPhysics().toString());
			jsonObject.addProperty("chemistry", facultyFeedback.getChemistry().toString());
			jsonObject.addProperty("math", facultyFeedback.getMath().toString());
			jsonObject.addProperty("biology", facultyFeedback.getBiology().toString());
			jsonObject.addProperty("createdDate", CommonUtils.getUAETimestamp(facultyFeedback.getCreatedDate()));
			fullName = facultyFeedback.getUser().getFirstName() + 
					(facultyFeedback.getUser().getMiddleName()==null?"":" "+facultyFeedback.getUser().getMiddleName()) + 
					" "+facultyFeedback.getUser().getLastName();
			jsonObject.addProperty("createdBy", fullName);
			
			jsonArray.add(jsonObject);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	
	
	
}
