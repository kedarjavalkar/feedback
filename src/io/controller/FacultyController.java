package io.controller;

import io.bi.faculty.FacultyManager;
import io.entity.Faculty;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Controller
public class FacultyController {

	@Autowired
	private  FacultyManager facultyManager;
	
	@RequestMapping(value="getFacultyList.io",method=RequestMethod.GET)
	@ResponseBody String getFacultyList(HttpServletRequest request) {
		List<Faculty> list = facultyManager.getAllActive();

		JsonArray jsonArray = new JsonArray();
		for (Faculty faculty : list) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("facultyId", faculty.getId().toString());
			String fullName = faculty.getFirstName() + (faculty.getMiddleName()==null?"":" "+faculty.getMiddleName()) + " "+faculty.getLastName();
			jsonObject.addProperty("facultyName", fullName);
			jsonArray.add(jsonObject);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="getAllFacultyList.io",method=RequestMethod.GET)
	@ResponseBody String getAllFacultyList(HttpServletRequest request) {
		List<Faculty> list = facultyManager.getAll();

		JsonArray jsonArray = new JsonArray();
		for (Faculty faculty : list) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("facultyId", faculty.getId().toString());
			String fullName = faculty.getFirstName() + (faculty.getMiddleName()==null?"":" "+faculty.getMiddleName()) + " "+faculty.getLastName();
			jsonObject.addProperty("facultyName", fullName);
			jsonObject.addProperty("isActive", faculty.getIsActive()==true?"Active":"Inactive");
			jsonArray.add(jsonObject);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="editFaculty.io",method=RequestMethod.GET)
	@ResponseBody String editFaculty(HttpServletRequest request) {

		String facultyId = request.getParameter("facultyId");
		
		Faculty faculty = facultyManager.getById(Integer.parseInt(facultyId));

		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("facultyId", faculty.getId().toString());
		jsonObject.addProperty("firstName", faculty.getFirstName());
		jsonObject.addProperty("middleName", (faculty.getMiddleName()==null)?"":faculty.getMiddleName());
		jsonObject.addProperty("lastName", faculty.getLastName());
		jsonObject.addProperty("isActive", faculty.getIsActive()==true?"Active":"Inactive");
		
		return new Gson().toJson(jsonObject);
	}
	
	@RequestMapping(value="saveOrUpdateFaculty.io",method=RequestMethod.POST)
	@ResponseBody String saveOrUpdateFaculty(HttpServletRequest request) {

		String facultyId = request.getParameter("facultyId");
		String firstName = request.getParameter("firstName");
		String middleName = request.getParameter("middleName");
		String lastName = request.getParameter("lastName");
		String isActive = request.getParameter("isActive");
		
		Faculty faculty;
		if (facultyId != null)
			faculty = facultyManager.getById(Integer.parseInt(facultyId));
		else
			faculty = new Faculty();
		
		if (firstName != null)
			faculty.setFirstName(firstName);
		if (middleName != null)
			faculty.setMiddleName(middleName);
		if (lastName != null)
			faculty.setLastName(lastName);
		faculty.setIsActive(isActive.equalsIgnoreCase("true") ? true : false);
		
		facultyManager.save(faculty);
		
		return "Batch change accepted";
		
	}
	
}
