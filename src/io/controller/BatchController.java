package io.controller;

import io.bi.batch.BatchManager;
import io.entity.Batch;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Controller
public class BatchController {

	@Autowired
	BatchManager batchManager;
	
	@RequestMapping(value="getBatchList.io",method=RequestMethod.GET)
	@ResponseBody String getBatchList(HttpServletRequest request) {
		List<Batch> list = batchManager.getAllActive();

		JsonArray jsonArray = new JsonArray();
		for (Batch batch : list) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("batchId", batch.getId().toString());
			jsonObject.addProperty("batchName", batch.getName());
			jsonArray.add(jsonObject);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="getAllBatchList.io",method=RequestMethod.GET)
	@ResponseBody String getAllBatchList(HttpServletRequest request) {
		List<Batch> list = batchManager.getAll();

		JsonArray jsonArray = new JsonArray();
		for (Batch batch : list) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("batchId", batch.getId().toString());
			jsonObject.addProperty("name", batch.getName());
			jsonObject.addProperty("isActive", batch.getIsActive() ? "Active" : "Inactive");
			jsonArray.add(jsonObject);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="editBatch.io",method=RequestMethod.GET)
	@ResponseBody String editBatch(HttpServletRequest request) {

		String batchId = request.getParameter("batchId");
		
		Batch batch = batchManager.getById(Integer.parseInt(batchId));
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("batchId", batch.getId().toString());
		jsonObject.addProperty("name", batch.getName());
		jsonObject.addProperty("isActive", batch.getIsActive() ? "Active" : "Inactive");

		return new Gson().toJson(jsonObject);
	}
	
	@RequestMapping(value="saveOrUpdateBatch.io",method=RequestMethod.POST)
	@ResponseBody String saveOrUpdateBatch(HttpServletRequest request) throws ParseException {

		String batchId = request.getParameter("batchId");
		String name = request.getParameter("name");
		String isActive = request.getParameter("isActive");
		
		Batch batch;
		
		if (batchId != null)
			batch = batchManager.getById(Integer.parseInt(batchId));
		else
			batch = new Batch();
			
		if (name != null)
			batch.setName(name);
		batch.setIsActive(isActive.equalsIgnoreCase("true") ? true : false);
		
		batchManager.save(batch);
		
		return "Batch change accepted";
		
	}

}