package io.controller;

import io.bi.user.UserManager;
import io.entity.Role;
import io.entity.User;
import io.utils.FeedbackException;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


@Controller
public class UserController {
	
	private static final String PASSWORD = "meccademia-feedback";

	@Autowired
	UserManager userManager;
	
	@RequestMapping(value="getAllUserList.io",method=RequestMethod.GET)
	@ResponseBody String getAllUserList(HttpServletRequest request) {
		List<User> list = userManager.getAll();

		JsonArray jsonArray = new JsonArray();
		for (User user : list) {
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("userId", user.getId().toString());
			String fullName = user.getFirstName() + (user.getMiddleName()==null?"":" "+user.getMiddleName()) +
					" " + user.getLastName();
			jsonObject.addProperty("name", fullName);
			jsonObject.addProperty("loginId", user.getLoginName());
			jsonObject.addProperty("isActive", user.getIsActive()==true?"Active":"Inactive");
			jsonArray.add(jsonObject);
		}
		
		return new Gson().toJson(jsonArray);
	}
	
	@RequestMapping(value="editUser.io",method=RequestMethod.GET)
	@ResponseBody String editUser(HttpServletRequest request) {

		String userId = request.getParameter("userId");
		User user = userManager.getById(Integer.parseInt(userId));

		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("userId", user.getId().toString());
		jsonObject.addProperty("firstName", user.getFirstName());
		jsonObject.addProperty("middleName", user.getMiddleName());
		jsonObject.addProperty("lastName", user.getLastName());
		jsonObject.addProperty("loginId", user.getLoginName());
		jsonObject.addProperty("isActive", user.getIsActive()==true?"Active":"Inactive");
		
		return new Gson().toJson(jsonObject);
	}
	
	@RequestMapping(value="saveOrUpdateUser.io",method=RequestMethod.POST)
	@ResponseBody String saveOrUpdateUser(HttpServletRequest request) {

		String userId = request.getParameter("userId");
		String firstName = request.getParameter("firstName");
		String middleName = request.getParameter("middleName");
		String lastName = request.getParameter("lastName");
		String loginId = request.getParameter("loginId");
		String isActive = request.getParameter("isActive");
		String action = request.getParameter("action");
		
		User user;
		if (userId != null)
			user = userManager.getById(Integer.parseInt(userId));
		else{
			user = new User();
			Role role = new Role();
			role.setId(2);
			user.setRole(role);
		}
		if(firstName!=null)
			user.setFirstName(firstName);
		if(middleName!=null)
			user.setMiddleName(middleName);
		if(lastName!=null)
			user.setLastName(lastName);
		if(loginId!=null)
			user.setLoginName(loginId);
		if(isActive!=null)
			user.setIsActive(isActive.equalsIgnoreCase("true")?true:false);
		if(action.equalsIgnoreCase("Save"))
			user.setPassword(PASSWORD);
		
		try {
			userManager.save(user);
		} catch (FeedbackException e) {
			return e.getMessage();
		}
		
		if(action.equalsIgnoreCase("Save"))
			return "Default password : <b>" + PASSWORD + "</b>";

		return "User change accepted";
	}
	
	@RequestMapping(value="updatePassword.io",method=RequestMethod.POST)
	@ResponseBody String changePassword(HttpServletRequest request) throws FeedbackException {
		
		JsonObject jsonObject = new JsonObject();
		
		String oldPassword = request.getParameter("oldPassword");
		String newPassword = request.getParameter("newPassword");
		Object loginId = request.getSession().getAttribute("mecca-feedback-userId");
		
		User user = new User();
		if(loginId!=null)
			user = userManager.getById(Integer.parseInt(loginId.toString()));
		
		if(user.getPassword().equals(oldPassword)) {
			user.setPassword(newPassword);
			userManager.save(user);
		} else {
			jsonObject.addProperty("Error", "Old password did not match");
			return new Gson().toJson(jsonObject);
		}
		
		jsonObject.addProperty("Success", "Password Changed");
		return new Gson().toJson(jsonObject);
	}
	
	
	
}
