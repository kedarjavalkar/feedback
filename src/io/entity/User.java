package io.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_t")
public class User implements Serializable {

	private static final long serialVersionUID = -4826682993977494208L;

	@Id
	@GeneratedValue
	@Column(name="user_id")
	private Integer id;

	@Column(name="first_name", length=25, nullable = false)
	private String firstName;
	
	@Column(name="middle_name", length=25)
	private String middleName;

	@Column(name="last_name", length=50, nullable = false)
	private String lastName;

	@Column(name="login_name", length=50, nullable = false)
	private String loginName;

	@Column(name="password", length=25, nullable = false)
	private String password;

	@Column(name="is_active", nullable = false)
	private Boolean isActive;
	
	@ManyToOne
	@JoinColumn(name ="role_id")
	private Role role;

	/**************** Getters and Setters **********************************/	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
