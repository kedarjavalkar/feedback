package io.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="faculty_t")
public class Faculty implements Serializable {

	private static final long serialVersionUID = 2633732744667825489L;

	@Id
	@GeneratedValue
	@Column(name="faculty_id")
	private Integer id;
	
	@Column(name="first_name", length=25, nullable = false)
	private String firstName;
	
	@Column(name="middle_name", length=25)
	private String middleName;

	@Column(name="last_name", length=50, nullable = false)
	private String lastName;

	@Column(name="is_active", nullable = false)
	private Boolean isActive;
	
	/**************** Getters and Setters **********************************/	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
}
