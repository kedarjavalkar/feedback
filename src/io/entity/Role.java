package io.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="role_t")
public class Role implements Serializable {

	private static final long serialVersionUID = -5512105044413223384L;

	@Id
	@GeneratedValue
	@Column(name="role_id")
	private Integer id;

	@Column(name="name", length=25, nullable = false)
	private String name;

	/**************** Getters and Setters **********************************/	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
