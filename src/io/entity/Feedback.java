package io.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="feedback_t")
public class Feedback implements Serializable {

	private static final long serialVersionUID = 2622146605792946569L;
	
	@Id
	@GeneratedValue
	@Column(name="feedback_id")
	private Integer id;

	@Column(name="comment", length=256, nullable = false)
	private String comment;
	
	@Column(name="is_active", nullable = false)
	private Boolean isActive;
	
	/**************** Getters and Setters **********************************/	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
}
