package io.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="faculty_feedback_t")
public class FacultyFeedback implements Serializable {

	private static final long serialVersionUID = 6447214121128999782L;

	@Id
	@GeneratedValue
	@Column(name="faculty_feedback_id")
	private Integer id;
	
	@Column(name="studentName", nullable = false)
	private String studentName;
	
	@ManyToOne
	@JoinColumn(name ="batch_id")
	private Batch batch;
	
	@ManyToOne
	@JoinColumn(name ="feedback_schedule_id")
	private FeedbackSchedule feedbackSchedule;
	
	@ManyToOne
	@JoinColumn(name ="faculty_id")
	private Faculty faculty;
	
	@ManyToOne
	@JoinColumn(name ="feedback_id")
	private Feedback feedback;
	
	@Column(name="physics", nullable = false)
	private Integer physics;

	@Column(name="chemistry", nullable = false)
	private Integer chemistry;

	@Column(name="math", nullable = false)
	private Integer math;
	
	@Column(name="biology", nullable = false)
	private Integer biology;
	
	@Column(name="created_date", nullable = false)
	private Date createdDate;
	
	@ManyToOne
	@JoinColumn(name ="user_id")
	private User user;
	
	/**************** Getters and Setters **********************************/

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Batch getBatch() {
		return batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public FeedbackSchedule getFeedbackSchedule() {
		return feedbackSchedule;
	}

	public void setFeedbackSchedule(FeedbackSchedule feedbackSchedule) {
		this.feedbackSchedule = feedbackSchedule;
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public Feedback getFeedback() {
		return feedback;
	}

	public void setFeedback(Feedback feedback) {
		this.feedback = feedback;
	}

	public Integer getPhysics() {
		return physics;
	}

	public void setPhysics(Integer physics) {
		this.physics = physics;
	}

	public Integer getChemistry() {
		return chemistry;
	}

	public void setChemistry(Integer chemistry) {
		this.chemistry = chemistry;
	}

	public Integer getMath() {
		return math;
	}

	public void setMath(Integer math) {
		this.math = math;
	}

	public Integer getBiology() {
		return biology;
	}

	public void setBiology(Integer biology) {
		this.biology = biology;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
