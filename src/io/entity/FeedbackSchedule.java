package io.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="feedback_schedule_t")
public class FeedbackSchedule implements Serializable {

	private static final long serialVersionUID = 8149953347681764488L;

	@Id
	@GeneratedValue
	@Column(name="feedback_schedule_id")
	private Integer id;
	
	@Column(name="schedule", nullable = false)
	private Date schedule;
	
	@Column(name="is_active", nullable = false)
	private Boolean isActive;

	/**************** Getters and Setters **********************************/	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Date getSchedule() {
		return schedule;
	}
	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
}
