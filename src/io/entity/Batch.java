package io.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="batch_t")
public class Batch implements Serializable {

	private static final long serialVersionUID = 2590128396760885876L;

	@Id
	@GeneratedValue
	@Column(name="batch_id")
	private Integer id;
	
	@Column(name="name", length=50, nullable = false)
	private String name;

	@Column(name="is_active")
	private Boolean isActive;
	
	/**************** Getters and Setters **********************************/	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
}
